﻿using System;
using System.Text;
using System.Threading;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace Consumer
{
    class Program
    {
        static void Main(string[] args)
        {
            string queueName = "carsQueue" + args[0];
            const string exchangeName = "newRouting";

            using (var connection = GetRabbitConnection())
            using (var channel = connection.CreateModel())
            {
                channel.BasicQos(0, 1, false);

                channel.QueueDeclare(queueName, false, false, false, null);
                channel.QueueBind(queueName, exchangeName, "cars"+args[0], null);

                var consumer = new EventingBasicConsumer(channel);

                consumer.Received += (sender, e) =>
                {
                    Console.WriteLine($"{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.ff")} Received message");

                    int timeToSleep = new Random().Next(30, 100);
                    Thread.Sleep(timeToSleep);

                    var body = e.Body;
                    var message = Encoding.UTF8.GetString(body.ToArray());
                    Console.WriteLine("  Received message: {0}", message);

                    channel.BasicAck(e.DeliveryTag, false);
                };

                channel.BasicConsume(queue: queueName,
                                     autoAck: false,
                                     consumer: consumer);

                Console.WriteLine("Subscribed to the queue");

                Console.ReadLine();
            }
        }

        static private IConnection GetRabbitConnection()
        {
            ConnectionFactory factory = new ConnectionFactory
            {
                UserName = "qfuqtywt",
                Password = "4-KC_Ze0o8oaOTBB3gtD7im2S2XJm2zk",
                VirtualHost = "qfuqtywt",
                HostName = "kangaroo.rmq.cloudamqp.com"
            };
            IConnection conn = factory.CreateConnection();
            return conn;
        }
    }
}
