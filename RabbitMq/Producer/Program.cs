﻿using System;
using System.Text;
using System.Threading;
using RabbitMQ.Client;

namespace Producer
{
    class Program
    {
        static void Main(string[] args)
        {
            const string exchangeName = "newRouting";

            var counter = 1;
            using (var connection = GetRabbitConnection())
            using (var channel = connection.CreateModel())
            {
                channel.ExchangeDeclare(exchangeName, ExchangeType.Direct);

                Console.WriteLine(connection.ChannelMax);
                do
                {
                    // counter = 0;
                    do
                    {
                        int timeToSleep = new Random().Next(10, 50);
                        Thread.Sleep(timeToSleep);

                        string message = $"Message from publisher N {counter + 1}";

                        var body = Encoding.UTF8.GetBytes(message);

                        channel.BasicPublish(exchange: exchangeName,
                                            routingKey: "cars" + (counter % 4).ToString(),
                                            basicProperties: null,
                                            body: body);

                        Console.WriteLine($"Message is sent into Default Exchange [N:{counter++}]");
                    } while (counter % 20 != 0);
                } while (Console.ReadKey().Key != ConsoleKey.Enter);
            }
        }

        static private IConnection GetRabbitConnection()
        {
            ConnectionFactory factory = new ConnectionFactory
            {
                UserName = "qfuqtywt",
                Password = "4-KC_Ze0o8oaOTBB3gtD7im2S2XJm2zk",
                VirtualHost = "qfuqtywt",
                HostName = "kangaroo.rmq.cloudamqp.com"
            };
            IConnection conn = factory.CreateConnection();
            return conn;
        }
    }
}
